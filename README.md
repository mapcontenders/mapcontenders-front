# MapContenders front client (mobile app + angular SPA)


## Dependencies

Deps are managed with npm.

Run `npm i` after cloning the repo to download deps. Use `--legacy-peer-deps` option if tree dependency errors

For upgrading npm packages to their latest version:

```bash
npm i -g npm-check-updates
ncu -u
npm i
# if tree dependency version errors;
npm i --legacy-peer-deps
```


## Dev

- Populate gitignored file `src/environments/environment.dev.ts` with accurate values
- Serve dev app with:
  - `ng serve -c dev`
  - `ionic serve -d dev`

(see `dev` and other configurations in angular.json)


## CICD

If using docker runners, make sure used images have all necessary tools.

Following dependencies are required on shell runners:

```bash
# npm
curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
apt update && apt install nodejs

# angular
npm i -g @angular/cli
# ionic
npm i -g @ionic/cli

# java + android
apt install openjdk-11-jdk gradle
apt-get install android-sdk*

# env vars
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export ANDROID_SDK_ROOT=/usr/lib/android-sdk
```

Accept Android licenses:

[stack overflow issue](https://stackoverflow.com/questions/53994924/sdkmanager-command-not-found-after-installing-android-sdk)

```bash
wget https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip
unzip commandlinetools-linux-6609375_latest.zip -d cmdline-tools
sudo mv cmdline-tools $ANDROID_SDK_ROOT/
export PATH=$PATH:$ANDROID_SDK_ROOT/cmdline-tools/tools/bin
```

It might also be needed to change unix rights of folder `$ANDROID_SDK_ROOT/build-tools` (wright needed for unix runner user, usually `gitlab-runner` user)



## Builds


### Ionic (mobile)

Set OS env variable `ANDROID_SDK_ROOT` pointing to where your Android SDK are, or use a `local.properties` file in android folder with SDK location like (for example: `sdk.dir=/home/guilhem/Android/Sdk`)

Typically: add `ANDROID_SDK_ROOT=$HOME/Android/Sdk` to your ~.profile (if Android installed with Android Studio) and create a file `android/local.properties` with `sdk.dir=$HOME/Android/Sdk`

#### Update Android cap

```bash
npm i [--legacy-peer-deps]
ng build --configuration production
npx cap update android
```

Copy ts edits (like capacitor.config.json for instance) to android:

```bash
npx cap copy
```

#### Build SDK


```bash
npm run android-build-release
```


### Angular (web)

- Serve dev with `ng serve -c dev` or `ionic serve -d dev` (see configurations in angular.json)
- Build prod with `ng build --configuration production` or `ionic build --prod`

Prod environment files should be secretly populated during CICD pipelines (`environments/environment.XXX.ts`, containing tokens and possible sensitive prod settings)

Use `configurations` parts in angular.json combined with `fileReplacements` to nicely handle environments
