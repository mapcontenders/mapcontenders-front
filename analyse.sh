#! /bin/bash

export PROJECT=mapcontenders-front

sonar-scanner \
  -Dsonar.hostUrl="$SONAR_HOST" \
  -Dsonar.projectName="$PROJECT" \
  -Dsonar.projectKey="$PROJECT" \
  -Dsonar.projectVersion="$NPM_VERSION" \
  -Dsonar.login="$SONAR_TOKEN" \
  -Dsonar.sources=src \
  -Dsonar.tests=src \
  -Dsonar.exclusions=**/node_modules/**,**/*.spec.ts,**/*test.ts,**/*.js,src/polyfills.ts \
  -Dsonar.test.inclusions=**/*.spec.ts,**/*test.ts \
  -Dsonar.coverage.exclusions=**/*.js,src/main.ts,src/polyfills.ts,**/*environment*.ts,**/*module.ts \
  -Dsonar.typescript.tsconfigPath=tsconfig.json \
  -Dsonar.javascript.lcov.reportPaths=coverage/lcov.info \
  -Dsonar.qualitygate.wait=true
