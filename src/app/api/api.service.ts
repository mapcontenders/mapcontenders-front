import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Place } from '@app/models/place.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pic2PointGuessSet } from '@app/models/pic2point-guess-set.model';
import { Point2PicGuessSet } from '@app/models/point2pic-guess-set.model';
import { Pic2PointScore } from '@app/models/pic2point-score.model';
import { Pic2PointGuessTry } from '@app/models/pic2point-guess-try.model';
import { Point2PicScore } from '@app/models/point2pic-score.model';

@Injectable({
    providedIn: 'root',
})
export class ApiService {
    constructor(private http: HttpClient) {}

    private buildUrl(resource: string): string {
        return `${environment.api.host}/api${resource}`;
    }

    // region place

    public getPlace(id: number): Observable<Place> {
        return this.http.get<any>(this.buildUrl(`/place/${id}`)).pipe(map((data) => new Place(data)));
    }

    public likePlace(id: number): Observable<Place> {
        return this.http.post<any>(this.buildUrl(`/place/${id}/like`), {}).pipe(map((data) => new Place(data)));
    }

    public dislikePlace(id: number): Observable<Place> {
        return this.http.post<any>(this.buildUrl(`/place/${id}/dislike`), {}).pipe(map((data) => new Place(data)));
    }

    public getPic2PointScores(id: number, count: number = 5): Observable<Array<Pic2PointScore>> {
        return this.http
            .get<any>(this.buildUrl(`/scores/${id}/pic2point?count=${count}`))
            .pipe(map((data) => data.map((d) => new Pic2PointScore(d))));
    }

    public getPoint2PicScores(id: number, count: number = 5): Observable<Array<Point2PicScore>> {
        return this.http
            .get<any>(this.buildUrl(`/scores/${id}/point2pic?count=${count}`))
            .pipe(map((data) => data.map((d) => new Point2PicScore(d))));
    }

    // endregion

    // region guess

    public getPic2PointGuessSet(difficulty?: number): Observable<Pic2PointGuessSet> {
        let url = this.buildUrl('/guess/pic2point');
        if (difficulty !== undefined) {
            url += `?difficulty=${difficulty}`;
        }
        return this.http.get<any>(url).pipe(map((data) => new Pic2PointGuessSet(data)));
    }

    public tryPic2Point(guess: Pic2PointGuessTry): Observable<Pic2PointScore> {
        return this.http.post<any>(this.buildUrl('/guess/pic2point'), guess).pipe(map((data) => new Pic2PointScore(data)));
    }

    public getPoint2PicGuessSet(difficulty?: number): Observable<Point2PicGuessSet> {
        let url = this.buildUrl('/guess/point2pic');
        if (difficulty !== undefined) {
            url += `?difficulty=${difficulty}`;
        }
        return this.http.get<any>(url).pipe(map((data) => new Point2PicGuessSet(data)));
    }

    public tryPoint2PicGuess(guess: Pic2PointGuessTry): Observable<Point2PicScore> {
        return this.http.post<any>(this.buildUrl('/guess/point2pic'), guess).pipe(map((data) => new Point2PicScore(data)));
    }

    // endregion
}
