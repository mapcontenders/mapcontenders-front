import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'play',
        pathMatch: 'full',
    },
    {
        path: 'play',
        loadChildren: () => import('./pages/play/play.module').then((m) => m.PlayPageModule),
    },
    {
        path: 'scores',
        loadChildren: () => import('./pages/scores/scores.module').then((m) => m.ScoresPageModule),
    },
    {
        path: 'places',
        loadChildren: () => import('./pages/places/places.module').then((m) => m.PlacesPageModule),
    },
    {
        path: 'about',
        loadChildren: () => import('./pages/about/about.module').then((m) => m.AboutPageModule),
    },
    {
        path: 'pic2point',
        loadChildren: () => import('./pages/play/pic2point.module').then((m) => m.Pic2PointPageModule),
    },
    {
        path: 'point2pic',
        loadChildren: () => import('./pages/play/point2pic.module').then((m) => m.Point2PicPageModule),
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
    exports: [RouterModule],
})
export class AppRoutingModule {}
