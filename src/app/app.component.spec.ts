import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { httpTranslateLoaderFactory } from '@app/app.module';

describe('AppComponent', () => {
    beforeEach(
        waitForAsync(() => {
            TestBed.configureTestingModule({
                declarations: [AppComponent],
                schemas: [CUSTOM_ELEMENTS_SCHEMA],
                imports: [HttpClientModule, RouterTestingModule.withRoutes([]), TranslateModule.forRoot()],
            }).compileComponents();
        })
    );

    it(
        'should create the app',
        waitForAsync(() => {
            const fixture = TestBed.createComponent(AppComponent);
            const app = fixture.debugElement.componentInstance;
            expect(app).toBeTruthy();
        })
    );

    it(
        'should have urls',
        waitForAsync(() => {
            const fixture = TestBed.createComponent(AppComponent);
            fixture.detectChanges();
            const app = fixture.nativeElement;
            const menuItems = app.querySelectorAll('ion-item');
            expect(menuItems.length).toEqual(4);
            expect(menuItems[0].getAttribute('ng-reflect-router-link')).toEqual('/play');
            expect(menuItems[1].getAttribute('ng-reflect-router-link')).toEqual('/scores');
            expect(menuItems[2].getAttribute('ng-reflect-router-link')).toEqual('/places');
        })
    );
});
