import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';
import { Platform } from '@ionic/angular';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
    readonly topPages: Array<{ titleKey: string; url: string; icon: string }> = [
        { titleKey: 'menu.play', url: '/play', icon: 'game-controller' },
        { titleKey: 'menu.scores', url: '/scores', icon: 'stats-chart' },
        { titleKey: 'menu.places', url: '/places', icon: 'locate' },
    ];
    readonly bottomPages: Array<{ titleKey: string; url: string; icon: string }> = [
        { titleKey: 'menu.about', url: '/about', icon: 'apps' },
    ];

    constructor(private platform: Platform, private titleService: Title, private translateService: TranslateService) {
        // constructor
    }

    ngOnInit(): void {
        this.platform.ready().then(() => {
            this.translateService.addLangs(['en', 'fr', 'de']);
            this.translateService.setDefaultLang('en');
            this.translateService.use(navigator.language);
            this.translateService.get('meta.title').subscribe((tr) => this.titleService.setTitle(tr));
        });
    }
}
