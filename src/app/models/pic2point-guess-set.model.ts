import { Picture } from '@app/models/picture.model';

export class Pic2PointGuessSet {
    public readonly created: Date;
    public readonly difficulty?: number;
    public readonly pictures: Array<Picture>;

    constructor(data: { created: Date; difficulty: number; pictures: Array<Picture> }) {
        this.created = data.created;
        this.difficulty = data.difficulty;
        this.pictures = data.pictures;
    }
}
