/* eslint-disable @typescript-eslint/naming-convention */

export class Pic2PointGuessTry {
    public readonly created: Date;
    public readonly difficulty?: number;
    public readonly id_pictures: Array<number>;
    public readonly guessed_lon: number;
    public readonly guessed_lat: number;

    constructor(created: Date, difficulty: number, id_pictures: Array<number>, guessed_lon: number, guessed_lat: number) {
        this.created = created;
        this.difficulty = difficulty;
        this.id_pictures = id_pictures;
        this.guessed_lon = guessed_lon;
        this.guessed_lat = guessed_lat;
    }
}
