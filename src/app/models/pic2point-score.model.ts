import { Place } from '@app/models/place.model';

export class Pic2PointScore {
    public readonly submitted: Date;
    public readonly difficulty?: number;
    public readonly duration: number;
    public readonly place: Place;
    public readonly distance: number;

    constructor(data: { submitted: Date; difficulty: number; duration: number; place: Place; distance: number }) {
        this.submitted = data.submitted;
        this.difficulty = data.difficulty;
        this.duration = data.duration;
        this.place = data.place;
        this.distance = data.distance;
    }
}
