export class Picture {
    public readonly id: number;
    public readonly creation: Date;
    public readonly url: string;

    constructor(data: { id: number; creation: Date; url: string }) {
        this.id = data.id;
        this.creation = data.creation;
        this.url = data.url;
    }
}
