import { Picture } from '@app/models/picture.model';

export class Place {
    public readonly id: number;
    public readonly name: string;
    public readonly hint: string;
    public readonly difficulty: number;
    public readonly creation: Date;
    public readonly creator: string;
    public readonly lon: number;
    public readonly lat: number;
    public readonly likes: number;
    public readonly dislikes: number;
    public readonly pictures: Array<Picture>;

    constructor(data: {
        id: number;
        name: string;
        hint: string;
        difficulty: number;
        creation: Date;
        creator: string;
        lon: number;
        lat: number;
        likes: number;
        dislikes: number;
        pictures: Array<any>;
    }) {
        this.id = data.id;
        this.name = data.name;
        this.hint = data.hint;
        this.difficulty = data.difficulty;
        this.creation = data.creation;
        this.creator = data.creator;
        this.lon = data.lon;
        this.lat = data.lat;
        this.likes = data.likes;
        this.dislikes = data.dislikes;
        this.pictures = data.pictures.map((dp) => new Picture(dp));
    }
}
