import { Picture } from '@app/models/picture.model';
import { Place } from '@app/models/place.model';

export class Point2PicGuessSet {
    public readonly created: Date;
    public readonly difficulty?: number;
    public readonly place: Place;
    public readonly pictures: Array<Picture>;

    constructor(data: { created: Date; difficulty: number; place: Place; pictures: Array<Picture> }) {
        this.created = data.created;
        this.difficulty = data.difficulty;
        this.place = data.place;
        this.pictures = data.pictures;
    }
}
