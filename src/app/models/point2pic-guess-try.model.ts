/* eslint-disable @typescript-eslint/naming-convention */

export class Point2PicGuessTry {
    public readonly created: Date;
    public readonly difficulty?: number;
    public readonly id_place: number;
    public readonly id_pictures: Array<number>;
    public readonly guessed_id_picture: number;

    constructor(created: Date, difficulty: number, id_place: number, id_pictures: Array<number>, guessed_id_picture: number) {
        this.created = created;
        this.difficulty = difficulty;
        this.id_place = id_place;
        this.id_pictures = id_pictures;
        this.guessed_id_picture = guessed_id_picture;
    }
}
