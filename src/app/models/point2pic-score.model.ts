import { Place } from '@app/models/place.model';

export class Point2PicScore {
    public readonly submitted: Date;
    public readonly difficulty?: number;
    public readonly duration: number;
    public readonly place: Place;
    public readonly success: boolean;

    constructor(data: { submitted: Date; difficulty: number; duration: number; place: Place; success: boolean }) {
        this.submitted = data.submitted;
        this.difficulty = data.difficulty;
        this.duration = data.duration;
        this.place = data.place;
        this.success = data.success;
    }
}
