import { Component } from '@angular/core';
import { version } from '@root/package.json';

@Component({
    selector: 'mc-about',
    templateUrl: './about.page.html',
    styleUrls: ['./about.page.scss'],
})
export class AboutPage {
    readonly appVersion = version;
    readonly maintainer = 'Guilhem Allaman';
    readonly email = 'mapcontenders@guilhemallaman.net';
    readonly repository = 'https://gitlab.com/mapcontenders';
}
