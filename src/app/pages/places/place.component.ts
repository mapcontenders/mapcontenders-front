import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Place } from '@app/models/place.model';
import { ApiService } from '@app/api/api.service';
import { Picture } from '@app/models/picture.model';
import { Pic2PointScore } from '@app/models/pic2point-score.model';
import { Point2PicScore } from '@app/models/point2pic-score.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'mc-place',
    templateUrl: './place.component.html',
    styleUrls: ['./place.component.scss'],
})
export class PlaceComponent implements OnInit {
    @Input()
    private id: number;
    place: Place;
    pictures: Array<Picture>;
    isLoading: boolean;
    hasLikedOrNot: boolean;

    scores: {
        pic2point: Array<Pic2PointScore>;
        point2pic: Array<Point2PicScore>;
    };

    slideOptions = {
        initialSlide: 0,
        speed: 500,
    };

    constructor(
        private api: ApiService,
        private modalController: ModalController,
        public toastController: ToastController,
        private translateService: TranslateService
    ) {
        this.isLoading = false;
        this.hasLikedOrNot = false;
        this.scores = {
            pic2point: [],
            point2pic: [],
        };
    }

    ngOnInit(): void {
        this.isLoading = true;
        this.api.getPlace(this.id).subscribe({
            next: (p: Place) => {
                this.place = p;
                this.pictures = p.pictures;
                this.api.getPic2PointScores(p.id).subscribe((scores) => (this.scores.pic2point = scores));
                this.api.getPoint2PicScores(p.id).subscribe((scores) => (this.scores.point2pic = scores));
            },
            complete: () => (this.isLoading = false),
            error: this.onApiError,
        });
    }

    close() {
        // can "dismiss" itself and optionally pass back data
        this.modalController.dismiss({ dismissed: true });
    }

    onImageWillLoad(event: any): void {
        this.isLoading = true;
    }

    onImageDidLoad(event: any): void {
        this.isLoading = false;
    }

    onApiError(): void {
        this.close();
        this.translateService.get('dialog.error').subscribe((tr) => {
            this.toastController.create({ message: tr, duration: 3000 }).then((t) => t.present());
        });
    }

    like(): void {
        this.api.likePlace(this.place.id).subscribe({
            next: (p: Place) => {
                this.place = p;
                this.hasLikedOrNot = true;
            },
            complete: () => console.log('Liked place #', this.id),
            error: this.onApiError,
        });
    }

    dislike(): void {
        this.api.dislikePlace(this.place.id).subscribe({
            next: (p: Place) => {
                this.place = p;
                this.hasLikedOrNot = true;
            },
            complete: () => console.log('Disliked place #', this.id),
            error: this.onApiError,
        });
    }
}
