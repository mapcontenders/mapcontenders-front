import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlacesPageRoutingModule } from './places-routing.module';

import { PlacesPage } from './places.page';
import { PlaceComponent } from '@app/pages/places/place.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, PlacesPageRoutingModule, TranslateModule.forChild()],
    declarations: [PlacesPage, PlaceComponent],
})
export class PlacesPageModule {}
