import { Component } from '@angular/core';
import { environment } from '@env/environment';
import { ModalController, Platform } from '@ionic/angular';
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import { PlaceComponent } from '@app/pages/places/place.component';
import * as mapboxgl from 'mapbox-gl';

@Component({
    selector: 'mc-places',
    templateUrl: './places.page.html',
    styleUrls: ['./places.page.scss'],
})
export class PlacesPage {
    private map: mapboxgl.Map;

    constructor(private platform: Platform, private modalController: ModalController) {
        this.platform.ready().then(() => setTimeout(() => this.initMap()));
    }

    private initMap() {
        console.log(`${environment.api.host}/mapbox/styles/mapbox.json`);
        // init map
        this.map = new mapboxgl.Map({
            accessToken: environment.mapbox.accessToken,
            container: 'map',
            style: `${environment.api.host}/mapbox/styles/mapbox.json`,
            zoom: 0,
            minZoom: 0,
            maxZoom: 10,
            center: [0, 0],
            attributionControl: false,
        });

        // custom map controls
        this.map.dragRotate.disable();
        this.map.addControl(
            new MapboxGeocoder({
                accessToken: environment.mapbox.accessToken,
                mapboxgl,
                collapsed: true,
                language: navigator.language,
            })
        );
        this.map.addControl(new mapboxgl.NavigationControl());

        // geolocation control
        const geolocateControl = new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true,
            },
            trackUserLocation: true,
            showAccuracyCircle: true,
            showUserLocation: true,
        });
        this.map.addControl(geolocateControl);

        // place click handlers
        const placeClickHandler = (e: mapboxgl.MapLayerMouseEvent) => {
            if (e.features.length > 0) {
                this.onPlaceClick(e.features[0].properties.id);
            }
        };
        this.map.on('click', 'easy-places', placeClickHandler);
        this.map.on('click', 'medium-places', placeClickHandler);
        this.map.on('click', 'hard-places', placeClickHandler);
    }

    private async onPlaceClick(id: number): Promise<void> {
        const modal = await this.modalController.create({
            component: PlaceComponent,
            componentProps: { id },
            swipeToClose: true,
        });
        await modal.present();
    }
}
