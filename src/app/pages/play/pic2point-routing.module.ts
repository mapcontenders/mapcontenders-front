import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Pic2PointPage } from './pic2point.page';

const routes: Routes = [
    {
        path: '',
        component: Pic2PointPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class Pic2PointPageRoutingModule {}
