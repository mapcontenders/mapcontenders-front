import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Pic2PointPageRoutingModule } from './pic2point-routing.module';

import { Pic2PointPage } from './pic2point.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, Pic2PointPageRoutingModule, TranslateModule.forChild()],
    declarations: [Pic2PointPage],
})
export class Pic2PointPageModule {}
