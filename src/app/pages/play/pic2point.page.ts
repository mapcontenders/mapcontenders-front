import { Component, OnInit } from '@angular/core';
import { Pic2PointGuessSet } from '@app/models/pic2point-guess-set.model';
import { ApiService } from '@app/api/api.service';

@Component({
    selector: 'mc-pic2point',
    templateUrl: './pic2point.page.html',
    styleUrls: ['./pic2point.page.scss'],
})
export class Pic2PointPage implements OnInit {
    set: Pic2PointGuessSet;

    constructor(private apiService: ApiService) {}

    ngOnInit() {
        this.apiService.getPic2PointGuessSet().subscribe((set) => (this.set = set));
    }
}
