import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'mc-play',
    templateUrl: './play.page.html',
    styleUrls: ['./play.page.scss'],
})
export class PlayPage implements OnInit {
    constructor(private router: Router) {
        // constructor
    }

    ngOnInit() {
        // on init method
    }

    playPic2Point(): void {
        this.router.navigate(['/pic2point']);
    }

    playPoint2Pic(): void {
        this.router.navigate(['/point2pic']);
    }
}
