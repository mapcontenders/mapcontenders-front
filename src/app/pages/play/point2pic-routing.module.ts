import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Point2PicPage } from './point2pic.page';

const routes: Routes = [
    {
        path: '',
        component: Point2PicPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class Point2PicPageRoutingModule {}
