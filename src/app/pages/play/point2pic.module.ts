import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Point2PicPageRoutingModule } from './point2pic-routing.module';

import { Point2PicPage } from './point2pic.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, Point2PicPageRoutingModule, TranslateModule.forChild()],
    declarations: [Point2PicPage],
})
export class Point2PicPageModule {}
