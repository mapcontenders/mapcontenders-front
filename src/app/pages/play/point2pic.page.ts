import { Component, OnInit } from '@angular/core';
import { Point2PicGuessSet } from '@app/models/point2pic-guess-set.model';
import { ApiService } from '@app/api/api.service';

@Component({
    selector: 'mc-point2pic',
    templateUrl: './point2pic.page.html',
    styleUrls: ['./point2pic.page.scss'],
})
export class Point2PicPage implements OnInit {
    set: Point2PicGuessSet;

    constructor(private apiService: ApiService) {}

    ngOnInit() {
        this.apiService.getPoint2PicGuessSet().subscribe((set) => (this.set = set));
    }
}
