import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ScoresPage } from '@app/pages/scores/scores.page';
import { ScoresPageRoutingModule } from '@app/pages/scores/scores-routing.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule, ScoresPageRoutingModule, TranslateModule.forChild()],
    declarations: [ScoresPage],
})
export class ScoresPageModule {}
