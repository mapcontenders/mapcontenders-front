import { Component } from '@angular/core';

@Component({
    selector: 'mc-scores',
    templateUrl: './scores.page.html',
    styleUrls: ['./scores.page.scss'],
})
export class ScoresPage {}
