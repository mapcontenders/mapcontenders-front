export const environment = {
    production: true,
    api: {
        host: 'https://api.mapcontenders.guilhemallaman.net',
    },
    mapbox: {
        accessToken: 'pk.eyJ1IjoiZ3VpbGhlbWFsbGFtYW4iLCJhIjoiY2tvaDE4aDVrMDZyOTJ4cm1iN2d2cmdvZSJ9.QGZWNRs4nqojocpp5eQF9w',
        baseStyle: 'https://guilhem-street-map.guilhemallaman.net/styles/klokantech-basic/style.json',
    },
};
