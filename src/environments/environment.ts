export const environment = {
    production: false,
    api: {
        host: 'http://localhost:5000',
    },
    mapbox: {
        accessToken: 'pk.eyJ1IjoiZ3VpbGhlbWFsbGFtYW4iLCJhIjoiY2tvcjAyMWo4MHRrdDJub2ptb3V3MGo1YiJ9.lVrNDQR1faFNe_9wA_ngrQ',
        baseStyle: 'https://guilhem-street-map.guilhemallaman.net/styles/klokantech-basic/style.json',
    },
};
